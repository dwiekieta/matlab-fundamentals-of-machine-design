clear all
close all
clc
continueFlag = 0;

%% I Zalozenia
Ms = 800;   % [Nm] moment nominalny
w = 60;     % [rad/s] predkosc katowa 
dp = 0.25;  % [m] srednica wienca zebatego
L = 0.9;    % [m] dlugosc walu
L1 = 0.2;   % [m] dlugosc odcinka sprzeglo - podpora A
L2 = 0.25;  % [m] dlugosc odcinka podpora A - wieniec zebaty
L3 = 0.45;  % [m] dlugosc odcinka wieniec zebaty - podpora B

k1 = 0.3;   % silnik elektryczny
k2 = 1;     % pompa

alpha = degtorad(20);   % [rad] kat przyporu
alpha_w = degtorad(20); % [rad] toczny kat zarysu
beta = 0;               % [rad] kat pochylenia zebow

a = 0.002;  % [m] odleglosc miedzy czolami zebow

Fp = 50 * 10^-6; % [m] blad podzialki obwodowej

L_vec = 0:0.001:L;    % wektor dlugosci walu
L_vec_len = length(L_vec);

fi_vec = 0:degtorad(1):degtorad(180); % [rad] wektor kata styku
fi_vec_len = length(fi_vec);

fi_dop_m = 0.005;       % [rad/m] dopuszczalny kat skretu walu
fi_dop = fi_dop_m * L;  % [rad]

m = 0.01;       % [m] modul kola zebatego
y_dop = m*10^-3; %[m] maksymalne ugiecie walu

ms = 4;     % [kg] masa sprzegla
g = 9.81;   % [m/s^2] przyspieszenie ziemskie

%% II Rozpatrywane stale 
%       Re - Rm - E - Kr - Kc - Ks - Kgj - Kh
%   M1
%   M2
%   ...
%
% Re - granica plastycznosci [Pa]
% Rm - wytrzymalosc na rozciaganie [Pa]
% Kr - naprezenie dopuszczalne na stale rozciaganie [Pa]
% Kc - naprezenie dopuszczalne na stale sciskanie [Pa]
% Ks - naprezenie dopuszczalne na stale skrecanie i sciskanie [Pa]
% Kgj - naprezenie dopuszczalne na jednostronne zginanie tnace [Pa]
% Kh - naprezenia dopuszczalne [Pa]
% Kd - dopuszcalny nacisk powierzchniowy [Pa]

StalC15 = 1;
StalC45 = 2;
StalC45H = 3;
StalE275 = 4;
StalE295 = 5;
StalE335 = 6;
StalE360 = 7;

stalNames = {'C15', 'C45', 'C45H', 'E275', 'E295', 'E335', 'E360'};

% 1. Definicja podstawowych wlasnosci stali [Re - Rm - E]
StalReRmE = [320*10^6, 550*10^6, 210*10^9;      % C15
    490*10^6, 700*10^6, 210*10^9;               % C45
    110*10^7, 120*10^7, 210*10^9;               % C45H
    275*10^6, 440*10^6, 210*10^9;               % E275
    295*10^6, 490*10^6, 210*10^9;               % E295
    335*10^6, 590*10^6, 210*10^9;               % E335
    360*10^6, 690*10^6, 210*10^9;               % E360
    %230*10^6, 750*10^6, 209*10^9;               % 1.4301          
    %240*10^6, 680*10^6, 209*10^9              % 1.4401
    ];

% 2. Wyznaczenie naprezen krytycznych
LiczbaStali = length(StalReRmE(:,1));

Kr = 0.55;
Kc = 0.55;
Ks = 0.33;
Kgj = 0.35;
Kh = 950*10^6;
Kd = 0.5;
Kg = 0.6;

StalRe = 1;
StalRm = 2;
StalE = 3;
StalKr = 4;
StalKc = 5;
StalKs = 6;
StalKgj = 7;
StalKh = 8;
StalKd = 9;
StalKg = 10;

% 3. Macierz pelnych wlasnosci stali
Stal=[];
for i = 1:LiczbaStali
    Stal(i,:) = [StalReRmE(i,1), StalReRmE(i,2), StalReRmE(i,3), ...
        StalReRmE(i,1)*Kr, StalReRmE(i,1)*Kc, StalReRmE(i,1)*Ks, ...
        StalReRmE(i,1)*Kgj, Kh, StalReRmE(i,1)*Kd, StalReRmE(i,1)*Kg];
end

% 4. Wspolczynnik Poissona dla stali
Ni = 0.3;

%% III Wielkosci standardowe
% 1. Modul kola zebatego
m_standard = [1, 1.25, 1.5, 2, 2.5, 3, 4, 5, 6, 8, 10, 12, 16, 20, 25,...
    32, 40, 50, 80, 100] * 10^-3;
m_standard_len = length(m_standard);

% 2. Liczba zebow
z_standard = [20, 25, 30, 40, 60, 80, 100];
z_standard_len = length(z_standard);

% 3. Srednice podzialowe
dp_standard = m_standard'*z_standard;
dp_standard_vector = unique(sort(reshape(dp_standard,1,[])),'first');
dp_standard_vector_len = length(dp_standard_vector);

% 4. Srednice standardowe
d_standard = [6,7,8,9,10,11,12,14,16,18,19,20,22,24,25,28,30,32,35,38,...
    40,42,43,45,46,47,48,50,52,53,55,58,60,63,65,70,73,75,78,80,85,90,...
    95,100,110,120,125,130,140,150,160,170,180,190,200,220,240,250,260,...
    280,300,320,340,360,380,400,420,440,450,460,480,500,530,560,600,...
    630]*10^-3;
d_standard_len = length(d_standard);

% 5. Wspolczynnik K1
K1 = [5, 7.5, 9, 13, 19, 20, 22];

%% IV Obliczenia
% 1. Obliczenie sily obwodowej na kole zebatym - wzor 16
Pw = 2*Ms/dp;

% 2. Obliczneie ciezaru sprzegla
qs = ms*g; 

%% V Wstepne dobranie srednicy walu
wb = waitbar(0, 'Obliczenia wst�pne');
wb_total = waitbar(0, 'Total progress');
total_loops = fi_vec_len +(fi_vec_len*L_vec_len)+2*L_vec_len;

qp_x = [];
qp_y = [];

% 1. Obliczenie sil skladowych - wzor 17 i 18
for fi = 1:fi_vec_len
    qp_x(fi) = Pw*sin(fi_vec(fi));
    qp_y(fi) = Pw*cos(fi_vec(fi));
    
    waitbar((fi/fi_vec_len), wb,...
        'Obliczenia wst�pne: obliczenie si� sk�adowych');
    waitbar((fi/total_loops), wb_total);
end

wb_total_progress = fi_vec_len;

% 2. Reakcje w podporach
Rb_x = qp_x*L2/(L2+L3);
Rb_y = (qs*L1+qp_y*L2)/(L2+L3);

Ra_x = qp_x-Rb_x;
Ra_y = qs+qp_y-Rb_y;

% 3. Moment gn�cy
Mg_x = [];
Mg_y = [];

for fi = 1:fi_vec_len
    for l = 1:L_vec_len
        if L_vec(l) <= L1
            Mg_x(fi,l) = 0;
            Mg_y(fi,l) = qs*(L1-L_vec(l));
        elseif L_vec(l) <= (L1+L2)
                Mg_x(fi,l) = Ra_x(fi)*(L_vec(l)-L1);
                Mg_y(fi,l) = qs*(L1-L_vec(l))+Ra_y(fi)*(L_vec(l)-L1);
        else 
            Mg_x(fi,l) = Ra_x(fi)*(L_vec(l)-L1)-qp_x(fi)*(L_vec(l)-L1-L2);
            Mg_y(fi,l) = qs*(L1-L_vec(l))+Ra_y(fi)*(L_vec(l)-L1)-qp_y(fi)...
                *(L_vec(l)-L1-L2);
        end
    end
    waitbar((fi*L_vec_len/fi_vec_len/L_vec_len), wb,...
        'Obliczenia wst�pne: obliczenie moment�w gn�cych');
    wb_total_progress = wb_total_progress + L_vec_len;
    waitbar((wb_total_progress/total_loops), wb_total);
end

% 4. Moment zlozony
Mg_xy = sqrt(Mg_x.^2+Mg_y.^2);
Mg_xy_vec = Mg_xy(1,:);

% 5. Sila scinajaca
T_xy = [];
for t = 2:L_vec_len
    T_xy(t) = (Mg_xy(1,t)-Mg_xy(1,t-1))/(L_vec(t)-L_vec(t-1));
end
T_xy(1) = T_xy(2);

wb_total_progress = wb_total_progress + L_vec_len;
waitbar((wb_total_progress/total_loops), wb_total);

% 6. Moment skrecajacy
Ms_l = [];
for l = 1:L_vec_len
    if L_vec(l) < (L1+L2)
        Ms_l(l) = Ms;
    else
        Ms_l(l) = 0;
    end
end

wb_total_progress = wb_total_progress + L_vec_len;
waitbar((wb_total_progress/total_loops), wb_total);

figure(1)
title("Si�y sk�adowe w r�nych punktach przy�o�enia");
xlabel("k�t, rad")
ylabel("si�a, N")
hold on
grid on
plot(fi_vec, qp_x, 'r');
plot(fi_vec, qp_y, 'b');
legend('qp_x','qp_y')

figure(2)
title("Reakcje w podporach w r�nych punktach przy�o�enia");
xlabel("k�t, rad")
ylabel("si�a, N")
hold on
grid on
plot(fi_vec, Rb_x, 'r');
plot(fi_vec, Rb_y, 'b');
plot(fi_vec, Ra_x, '--r');
plot(fi_vec, Ra_y, '--b');
legend('Rb_x','Rb_y','Ra_x','Ra_y')

figure(3)
title("Momenty gn�ce w r�nych punktach przy�o�enia w p�aszczy�nie XZ");
xlabel("odleg�o��, m")
ylabel("k�t, rad")
zlabel("moment, Nm")
hold on
grid on
mesh(L_vec,fi_vec',Mg_x)
colorbar
xlim([min(L_vec) max(L_vec)])
ylim([min(fi_vec) max(fi_vec)])

figure(4)
title("Momenty gn�ce w r�nych punktach przy�o�enia w p�aszczy�nie YZ");
xlabel("odleg�o��, m")
ylabel("k�t, rad")
zlabel("moment, Nm")
hold on
grid on

mesh(L_vec,fi_vec',Mg_y)
colorbar
xlim([min(L_vec) max(L_vec)])
ylim([min(fi_vec) max(fi_vec)])

figure(5)
title("Momenty gn�ce w r�nych punktach przy�o�enia w p�aszczy�nie z�o�onej");
xlabel("odleg�o��, m")
ylabel("k�t, rad")
zlabel("moment, Nm")
hold on
grid on

mesh(L_vec,fi_vec',Mg_xy)
colorbar
xlim([min(L_vec) max(L_vec)])
ylim([min(fi_vec) max(fi_vec)])

figure(6)
title("Moment gn�cy w p�aszczy�nie z�o�onej");
xlabel("odleg�o��, m")
ylabel("moment, Nm")
hold on
grid on
plot(L_vec,Mg_xy_vec)

figure(7)
title("Si�a tn�ca wa� w p�aszczy�nie z�o�onej");
xlabel("odleg�o��, m")
ylabel("si�a, N")
hold on
grid on
plot(L_vec,T_xy)

figure(8)
title("Moment skr�caj�cy wa�");
xlabel("odleg�o��, m")
ylabel("moment, Nm")
hold on
grid on
plot(L_vec,Ms_l)

figure(9)
title("Por�wnanie moment�w dzia�aj�cych na wa�");
xlabel("odleg�o��, m")
ylabel("moment, Nm")
hold on
grid on
plot(L_vec,Mg_xy_vec)
plot(L_vec,Ms_l)
legend('Mg_{xy}', 'Ms')


% ---- ANALIZA ZREDUKOWANEGO NAPREZENIA NORMALNEGO -----
a = [];
sigma_dop = [];
M_zg = [];

for st = 1:LiczbaStali
    % 7. Obliczenie wspolczynnika a - wzor 5
    a(st) = Stal(st,StalKg)/Stal(st,StalKs);
    
    sigma_dop(st) = Stal(st,StalKg);
end

% 8. Obliczenie momentu zastepczego - wzor 8
M_zg = (Mg_xy_vec.^2 + (a(st)*Ms_l/2).^2).^(1/2);

% 9. Obliczenie srednic nominalnych - wzor 3 i 14
d_raw = [];
for i = 1:LiczbaStali
    d_raw(i,:) = (32*M_zg/pi/sigma_dop(i)).^(1/3);
end

% 10. Normalizacja srednic walu - zaleznosc 19
dn = [];
lD = 3;    % wsp l/d
dD_max = 1.2;
for i = 1:LiczbaStali
    d_max = max(d_raw(i,:));
    
    for l_max = 1:L_vec_len
        if d_raw(i,l_max) == d_max
            break;
        end
    end
    
    di = 1;
    % znalezienie maksymalnej srednicy
    for d = 1:d_standard_len
        if d_standard(d) > d_max
               dn(i,l_max) = d_standard(d);
            break;
        end
    end
    
    lp = L_vec(l_max);
    
    % szukanie w prawo
    for l = (l_max+1):L_vec_len
        if ((L_vec(l)-lp) < lD*dn(i,l-1)) && (lp ~= L_vec(l_max))
            dn(i,l) = dn(i,l-1);
            continue
        elseif (L_vec(l)-lp) < lD*dn(i,l-1)/2
            dn(i,l) = dn(i,l-1);
            continue
        end
        
        
        for ds = 1: d_standard_len
            if d_standard(ds) > d_raw(i,l)
                if dn(i,l-1)/d_standard(ds) > dD_max
                    continue
                end
                
                dn(i,l) = d_standard(ds);
                lp = L_vec(l);
                break;
            end
        end
    end
    
    lp = L_vec(l_max);
    
    % szukanie w lewo
    for l = (l_max-1):-1:1
        if ((lp-L_vec(l)) < lD*dn(i,l+1)) && (lp ~= L_vec(l_max))
            dn(i,l) = dn(i,l+1);
            continue
        elseif (lp-L_vec(l)) < lD*dn(i,l+1)/2
            dn(i,l) = dn(i,l+1);
            continue
        end
        
        for ds = 1: d_standard_len
            if d_standard(ds) > d_raw(i,l)
                if dn(i,l+1)/d_standard(ds) > dD_max
                    continue
                end
                
                dn(i,l) = d_standard(ds);
                lp = L_vec(l);
                break;
            end
        end
    end
    
end

figure(10)
title("Moment zast�pczy na wale");
xlabel("odleg�o��, m")
ylabel("moment, Nm")
hold on
grid on
plot(L_vec, M_zg)

figure(11)
title("�rednice nominalne wa�u w zale�no�ci od materia�u oraz momentu zredukowanego");
xlabel("odleg�o��, m")
ylabel("�rednica, m")
hold on
grid on
d_raw_plot = d_raw;
d_raw_plot(StalC15,:) = d_raw_plot(StalC15,:) * -1;
plot(L_vec,d_raw)
plot(L_vec,d_raw_plot(StalC15,:))
dn_plot = dn;
dn_plot(StalC15,:) = dn_plot(StalC15,:) * -1;
plot(L_vec,dn_plot(StalC15,:))
legend(stalNames{:}, 'Location', 'Best')
%plot(1:LiczbaStali,M_zg,'--', 'Color', [0.17 0.17 0.17])
%set(gca,'xtick',1:LiczbaStali,'xticklabel',stalNames)


%% VI Sztywnosc gietna walow ksztaltowanych
% znalezienie podpory
for li_L1 = 1:L_vec_len
    if L1 == L_vec(li_L1)
        break;
    end
end

% znalezeinie zebatki
for li_Z = 1:L_vec_len
    if (L1+L2) == L_vec(li_Z)
        break;
    end
end

% krok calkowania 
dz = L_vec(3)-L_vec(2);

% calka z Mg(z)dz ---------------------
C_Mg = [];
CC_Mg = [];
C_Mg(1) = 0;
CC_Mg(1) = 0;

for l = 2:L_vec_len
    C_Mg(l) = C_Mg(l-1)-(Mg_xy_vec(l-1)+Mg_xy_vec(l))/2*dz;
end

C = -C_Mg(li_L1);
C_Mg = C_Mg + C;

for l = 2:L_vec_len
    CC_Mg(l) = CC_Mg(l-1)+(C_Mg(l-1)+C_Mg(l))/2*dz+C*L_vec(l);
end

D = -CC_Mg(li_L1);
CC_Mg = CC_Mg + D;

% 1. Ustalenie przebiegu linii ugiecia
y = [];
for l = 1:L_vec_len
    if L_vec(l)<(L1+L2)
        y(l) = y_dop/L2*(L1-L_vec(l));
    else
        y(l) = y_dop*L_vec(l)/L3-y_dop/L3*(L);
    end
end

% 1. Linia ugiecia - wzor 23 -----------------------
CC_Mg_y = CC_Mg./abs(y);
d_raw_y = (abs(64*CC_Mg_y/Stal(StalC45,StalE)/pi)).^(1/4);
% obliczenie srednicy minimalnej - wzor 22
d_raw_theta = (64*C_Mg/Stal(StalC45,StalE)/pi/tan(fi_dop)).^(1/4);
% 2. Dopasowanie srednic
% obrot d_raw
d_raw_y_max = -max(d_raw_y);
d_raw_Y = (d_raw_y+d_raw_y_max)*-1;


figure(12)
title("Ca�ka z momentu z�o�onego");
xlabel("odleg�o��, m")
ylabel("moment, Nm^2")
hold on
grid on
plot(L_vec,C_Mg)
plot(L_vec,CC_Mg)
legend('C Mg','CC Mg')

figure(13)
title("Zarys �rednic minimalnych z warunku sztywno�ci gi�tnej wa�u");
xlabel("odleg�o��, m")
ylabel("�rednica, m")
hold on
grid on
%plot(L_vec,d_raw_theta)
plot(L_vec,d_raw_y)
legend('d_\theta','d_y')

figure(14)
title("Linia ugi�cia wa�u");
xlabel("odleg�o��, m")
ylabel("ugi�cie, m")
hold on
grid on
plot(L_vec,y)

%% VII Sztywnosc skretna
% 1. Przebieg skretu walu
fi_l = [];
for l = 1:L_vec_len
    if L_vec(l) < (L1+L2)
        fi_l(l) = fi_dop*(1-L_vec(l)/(L1+L2));
    else
        fi_l(l) = fi_dop;
    end
end

% 2. Obliczenie modulu Kirchoffa - wzor 25
G = Stal(1,StalE)/2/(1+Ni);

% 3. Obliczenie srednic granicznych - wzor 24
Ms_l_fi = Ms_l./fi_l;
d_raw_fi = (32*Ms_l_fi*dz/G/pi).^(1/4);


figure(15)
title("K�t sk�r�cania wa�u");
xlabel("odleg�o��, m")
ylabel("k�t, rad")
hold on
grid on
plot(L_vec,fi_l)

figure(16)
title("Zarys �rednic minimalnych z warunku sztywno�ci skr�tnej wa�u");
xlabel("odleg�o��, m")
ylabel("�rednica, m")
hold on
grid on
plot(L_vec,d_raw_fi)

%% VIII Drgania gietne
% 1. predkosc krytyczna
omega_kr = (g/y_dop)^(1/2);


close(wb);
close(wb_total);