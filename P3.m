clear all
clc
continueFlag = 0;

%% I Zalozenia
Mn = 3000;  % [Nm] moment nominalny
n = 2000;   % [obr/min] predkosc obrotowa 
Dr = 0.001; % [m] przemieszczenie promieniowe
Da = 0.002; % [m] przmieszczenie poosiowe
Dalpha = degtorad(1); % [rad] pochylenie osi 

k1 = 0.3;   % silnik elektryczny
k2 = 1;     % pompa

L = 0.25;   % [m] dlugosc walu

alpha = degtorad(20);   % [rad] kat przyporu
alpha_w = degtorad(20); % [rad] toczny kat zarysu
beta = 0;               % [rad] kat pochylenia zebow

a = 0.002;  % [m] odleglosc miedzy czolami zebow

Fp = 50 * 10^-6; % [m] blad podzialki obwodowej


%% II Rozpatrywane stale 
%       Re - Rm - E - Kr - Kc - Ks - Kgj - Kh
%   M1
%   M2
%   ...
%
% Re - granica plastycznosci [Pa]
% Rm - wytrzymalosc na rozciaganie [Pa]
% Kr - naprezenie dopuszczalne na stale rozciaganie [Pa]
% Kc - naprezenie dopuszczalne na stale sciskanie [Pa]
% Ks - naprezenie dopuszczalne na stale skrecanie i sciskanie [Pa]
% Kgj - naprezenie dopuszczalne na jednostronne zginanie tnace [Pa]
% Kh - naprezenia dopuszczalne [Pa]
% Kd - dopuszcalny nacisk powierzchniowy [Pa]

StalC45 = 2;
StalC45H = 3;

% 1. Definicja podstawowych wlasnosci stali [Re - Rm - E]
StalReRmE = [320*10^6, 550*10^6, 210*10^9;      % C15
    490*10^6, 700*10^6, 210*10^9;               % C45
    110*10^7, 120*10^7, 210*10^9;               % C45H
    230*10^6, 750*10^6, 209*10^9;               % 1.4301          
    240*10^6, 680*10^6, 209*10^9];              % 1.4401

% 2. Wyznaczenie naprezen krytycznych
LiczbaStali = length(StalReRmE(:,1));

Kr = 0.55;
Kc = 0.55;
Ks = 0.33;
Kgj = 0.35;
Kh = 950*10^6;
Kd = 0.5;

StalRe = 1;
StalRm = 2;
StalE = 3;
StalKr = 4;
StalKc = 5;
StalKs = 6;
StalKgj = 7;
StalKh = 8;
StalKd = 9;

% 3. Macierz pelnych wlasnosci stali
Stal=[];
for i = 1:LiczbaStali
    Stal(i,:) = [StalReRmE(i,1), StalReRmE(i,2), StalReRmE(i,3), ...
        StalReRmE(i,1)*Kr, StalReRmE(i,1)*Kc, StalReRmE(i,1)*Ks, ...
        StalReRmE(i,1)*Kgj, Kh, StalReRmE(i,1)*Kd];
end

% 4. Wspolczynnik Poissona dla stali
Ni = 0.3;

% 5. Dopuszczalny kat skretu walu 
Fi_dop = degtorad(0.5);

%% III Wielkosci standardowe
% 1. Modul kola zebatego
m_standard = [1, 1.25, 1.5, 2, 2.5, 3, 4, 5, 6, 8, 10, 12, 16, 20, 25,...
    32, 40, 50, 80, 100] * 10^-3;
m_standard_len = length(m_standard);

% 2. Liczba zebow
z_standard = [20, 25, 30, 40, 60, 80, 100];
z_standard_len = length(z_standard);

% 3. Srednice podzialowe
dp_standard = m_standard'*z_standard;
dp_standard_vector = unique(sort(reshape(dp_standard,1,[])),'first');
dp_standard_vector_len = length(dp_standard_vector);

% 4. Srednice standardowe
d_standard = [6,7,8,9,10,11,12,14,16,18,19,20,22,24,25,28,30,32,35,38,...
    40,42,43,45,46,47,48,50,52,53,55,58,60,63,65,70,73,75,78,80,85,90,...
    95,100,110,120,125,130,140,150,160,170,180,190,200,220,240,250,260,...
    280,300,320,340,360,380,400,420,440,450,460,480,500,530,560,600,...
    630]*10^-3;
d_standard_len = length(d_standard);

% 5. Wspolczynnik K1
K1 = [5, 7.5, 9, 13, 19, 20, 22];

%% IV Wstepne dobranie parametrow zebatki (6.3)
% 1. Obliczenie momentu oblcizeniowego - wzor 77
M_o = Mn*(k1+k2);

% 2. Wyznaczenie mocy silnika - wzor 96
N = M_o*2*pi*n/60;

% 3. Stosunek promieni - wzor 39
u = 1;

% 4. Szacowane wytezenie kola zebatego
Q_u_min = 1.9*10^6; % PKM III str. 415
Q_u_max = 2.6*10^6;

% 5. Przyjecie wsp. kappa
kappa_min = 0.1;      % PKM 1 tabela 11
kappa_max = 1.4;
kappa_step = (kappa_max-kappa_min)/500;

for kappa = kappa_min:kappa_step:kappa_max
    
    % 6. Obliczenie wstepnej srednicy wienca - wzor 73
    dp_wstepne = 270*(N*10^-3/n/kappa/Q_u_max/10^-6*(u+1)/u)^(1/3)*10^-3;
    
    % 7. Dobranie poczatkowej srednicy wienca
    for i = 1:dp_standard_vector_len
        if dp_standard_vector(i)>dp_wstepne
            dp_poczatkowe = i;
            break;
        end
    end
    
    %% V PETLA OBLICZENIOWA PROGRAMU
    for i = dp_poczatkowe:dp_standard_vector_len 
        
        %% V.1 Dobranie srednicy wienca
        dp = dp_standard_vector(i);
        
        %% V.2 Wyznaczenie szerokosci zeba - wzor 74
        b = kappa*dp;
        
        %% V.3 Wyznaczenie kombinacji tworzacych srednice podzialowa
        % V.3.1 Funkcja wyszukania
        dp_liczba = 0;
        dp_m = [];
        dp_z = [];
        
        for i_m = 1:m_standard_len
            for i_z = 1:z_standard_len
                if dp_standard(i_m,i_z) == dp
                    dp_liczba = dp_liczba + 1;
                    dp_m(dp_liczba) = m_standard(i_m);
                    dp_z(dp_liczba) = z_standard(i_z);
                end
            end
        end
        
        % V.3.2 Obliczenie sily obwodowej - wzor 72
        P_w = 19100*N*10^-3/n/dp;
        
        % V.3.2 Oblicznie momentu tarcia sily obwodowej - wzor 71
        T = P_w*dp/2;
        
        % V.3.3 Obliczenie wytezenia kola zebatego - wzor 70
        Q_u = 2*T*(u+1)/b/dp^2/u;
        
        % V.3.4 Sprawdzenie warunku wytezeniowego
        %   Jezeli jest spelniony to Q_u poza zakresem - nastepuje zwiekszenie
        %   srednicy dp_minimum
        if (Q_u_min > Q_u) || (Q_u_max < Q_u)
            disp(['Warunek V.3.4: Q_u = ' num2str(Q_u)...
                ', d_p = ' num2str(dp)...
                ', b = ' num2str(b)...
                ', kappa = ' num2str(kappa)]);
            continueFlag = 1;
            continue;
        end
        
        %% VI Dalsze obliczenia dla kazdej kombinacji parametrow m oraz z
        for i_dp = 1:dp_liczba
            %% VI.I Wytrzymalosc walcowego kola zebatego
            % I Teoria Hertza
            % 1.1 Wspolczynnik materialowy Zm - wzor 30
            Z_M = (Stal(StalC45H,StalE)/2/pi/(1-Ni^2))^(1/2);
            
            % 1.2 Obliczenie konta pochylenia zebow - wzor 32
            beta_b = acos((1-(sin(beta)*cos(alpha))^2)^(1/2));
            
            % 1.3 Wspolczynnik Zh - wzor 42
            Z_H = (2*cos(beta_b)*cos(alpha_w)/cos(alpha)^2/sin(alpha_w))...
                ^(1/2);
            
            % 1.4 Wspolczynnik Ka - wzor 33
            K_A = M_o/Mn;
            
            % 1.5 Wspolczynnik Kv - nierownosc 35
            K_v = 1.5;
            
            % 1.6 Wspoczynnik K_Hbeta - wzor 36
            K_Hbeta = 1.15;
            
            % 1.7 Wspolczynnik K_Halpha - wzor 37
            K_Halpha = 1;
            
            % 1.8 Oblcizenie naprezenia normalnego - wzor 41
            Sig_h = Z_M*Z_H*(P_w*(u+1)/dp/b/u)^(1/2)*(K_A*K_v*...
                K_Hbeta*K_Halpha)^(1/2);
            
            % 2.1 Podzialka zasadnicza - wzor 3
            p_b = dp_m(i_dp)*pi;
            
            % 2.2 Relacje miedzy zebami
            % 2.2.1.1 Kat polozenia zebow - wzor 19
            Fi_z = 2*pi/dp_z(i_dp)*(1:dp_z(i_dp) - 1);
            
            % 2.2.1.2 Znalezienie optymalnego zeba
            z_opt_czulosc = 0.15;
            for i_z = 1:(dp_z(i_dp)-1)
                Fi_za = Fi_z(i_z) - alpha;
                if (Fi_za > (pi/2 - pi/2*z_opt_czulosc)) &&...
                        (Fi_za < (pi/2 + pi/2*z_opt_czulosc))
                    z_opt = i_z;
                    break;
                end
                Fi_za = -1;
            end
            
            % 2.2.1.3 Warunek optymalnego kata
            if(Fi_za == -1)
                disp(['Warunek (VI.I.I)2.2.1.3: Fi_za = ' num2str(Fi_za)]);
                continueFlag = 1;
                continue;
            end
            
            % 2.2.2.1 Promien podzialowy
            r0 = dp/2;
            
            % 2.2.2.2 Luz boczny - wzor 20
            j_nk = r0*sin(Dalpha)/2/sin(alpha)*sin(Fi_za)^2+Fp*...
                (1-cos(Fi_za));
            
            % 2.2.3 Odleglosc punktow styku - wzor 21
            U2 = r0*sin(Dalpha)/sin(alpha)*cos(Fi_za);
            
            % 2.2.3 Wspolczynnik przemiszczenia zarysu - wzor 22
            x = dp_z(i_dp)/dp/sin(alpha)*((r0-(r0^2-U2^2)^(1/2))...
                *sin(alpha)-0.5*j_nk);
            
            % 2.2.4 Warunek istnienia x - zaleznosc 23
            h_a_wsp = 1;
            
            if (x < -h_a_wsp*0.6) || (x > h_a_wsp)
                disp(['Warunek (VI.I.I)2.2.4: dp = ' num2str(dp)...
                    ', m = ' num2str(dp_m(i_dp)) ', z = '...
                    num2str(dp_z(i_dp)) ', b = ' num2str(b) ', kappa = '...
                    num2str(kappa) ', j_nk = ' num2str(j_nk) ', U2 = '...
                    num2str(U2) ', x = ' num2str(x)]);
                
                continueFlag = 1;
                continue;
            end
            
            % 2.2.5 Warunek granicznej liczby zebow 
            % 2.2.5.1 Graniczna liczba zebow - wzor 24
            z_g = 2*(h_a_wsp - x)/sin(alpha)^2*1.1;
            
            % 2.2.5.2 Sprawdzenie warunku 25
            if dp_z(i_dp) < z_g
                disp(['Warunek (VI.I.I)2.2.5.2: dp = ' num2str(dp)...
                    ', m = ' num2str(dp_m(i_dp)) ', z = '...
                    num2str(dp_z(i_dp)) ', z_g = ' num2str(z_g)...
                    ', b = ' num2str(b) ', kappa = '...
                    num2str(kappa) ', j_nk = ' num2str(j_nk) ', U2 = '...
                    num2str(U2) ', x = ' num2str(x)]);
                
                continueFlag = 1;
                continue;
            end
            
            % 2.2.6 Wskaznik zazebienia
            % 2.2.6.1 Wspoczynnik luzu bocznego
            k = 0.01;
            
            % 2.2.6.2 Wysokosc glowy zeba - wzor 5
            h_a = (h_a_wsp+abs(x)-k)*dp_m(i_dp);
            
            % 2.2.6.3 Wspoczynnik luzu wierzcholkowego
            c_wsp = 0.25;
            
            % 2.2.6.4 Wysokosc stopy zeba - wzor 6
            h_f_wsp = h_a_wsp+c_wsp;
            
            h_f = (h_f_wsp-abs(x)+c_wsp)*dp_m(i_dp);
            
            % 2.2.6.5 Okrag wierzcholkowy - zwor 11
            d_a = dp+2*h_a;
            
            % 2.2.6.6 Okrag stop - wzor 12
            d_f = dp-2*h_f;
            
            % 2.2.6.7 Wskaznik zazebienia - wzor 27 i 26
            r_a = d_a/2;
            r_w = d_f/2;
            
            epsilon = 2*r_a/p_b*(((r_a/r_w)^2-cos(alpha)^2)^(1/2)...
                -sin(alpha));
            
            % 2.3 Wspolczynnik wskaznika zazebienia - wzor 45
            Z_epsilon = ((4-epsilon)/3)^(1/2);
            
            % 2.4 Wspolczynnik kata pochylenia zeba - wzor 46
            Z_beta = cos(beta)^(1/2);
            
            % 2.5 Naprezenie hertzowskie - wzor 44
            Sig_H0 = Z_H*Z_M*Z_epsilon*Z_beta*(P_w*(u+1)/dp/b/u)^(1/2);
            
            % 2.6 Naprezenie normalne - wzor 43
            Sig_H = Sig_H0*(K_A*K_v*K_Hbeta*K_Halpha)^(1/2);
            
            % 2.7 Naprezenie maksymalne
            % 2.7.1 Wspolczynniki - 48 do 54
            Z_NT = 1;
            Z_LRv = 0.92;
            Z_X = 1;
            S_H = 1.2;
            Sig_Hlim = 1.1*10^9;
            Z_W = 1.1;
            
            % 2.7.2 Naprezenie dopuszczalne - wzor 47
            Sig_HP = Sig_Hlim/S_H*Z_NT*Z_LRv*Z_W*Z_X;
            
            % 2.7.3 Sprawdzenie warunku 28
            if Sig_HP < Sig_H
                disp(['Warunek (VI.I.I)2.7.3: dp = ' num2str(dp) ', m = ' ...
                    num2str(dp_m(i_dp)) ', z = ' num2str(dp_z(i_dp)) ...
                    ', z_g = ' num2str(z_g)...
                    ', b = ' num2str(b) ', kappa = ' num2str(kappa) ...
                    ', j_nk = ' num2str(j_nk) ', U2 = ' num2str(U2) ...
                    ', x = ' num2str(x) ', h_a = ' num2str(h_a)...
                    ', h_f =' num2str(h_f) ', d_a = ' num2str(d_a)...
                    ', d_f = ' num2str(d_f) ', epsilon = ' num2str(epsilon)...
                    ', sig_H = ' num2str(Sig_H) ', sig_HP = ' ...
                    num2str(Sig_HP)]);
                
                continueFlag = 1;
                continue;
            end
            
            % II Zlamanie zmeczeniowe
            % 1. Parametry - 58 do 60
            Y_beta = 1;
            Y_Salpha = 1.6;
            Y_Falpha = 3;
            
            % 2. Wspolczynnik przyporu - wzor 61
            Y_epsilon = 0.25+0.75/epsilon*cos(beta_b)^2;
            
            % 3. Naprezenie bazowe - wzor 57
            Sig_F0 = P_w/b/dp_m(i_dp)*Y_Falpha*Y_Salpha*Y_epsilon*...
                Y_beta;
            
            % 4. Naprezenie obliczeniowe - wzor 56
            Sig_F = Sig_F0*K_A*K_v*K_Hbeta*K_Halpha;
            
            % 5. Naprezenie dopuszczalne
            % 5.1 Parametry - 63 do 69
            Y_ST = 2;
            Y_NT = 1;
            Y_dT = 0.95;
            Y_RT = 1.12;
            Y_X = 1;
            S_F = 1.5;
            Sig_Flim = 400*10^6;
            
            % 5.2 Naprezenie dopuszczalne - wzor 62
            Sig_FP = Sig_Flim/S_F*Y_ST*Y_NT*Y_dT*Y_RT*Y_X;
            
            % 5.3 Sprawdzenie warunku 55
            if Sig_FP < Sig_F                
                disp(['Warunek (VI.I.II)5.3: dp = ' num2str(dp) ', m = ' ...
                    num2str(dp_m(i_dp)) ', z = ' num2str(dp_z(i_dp)) ...
                    ', z_g = ' num2str(z_g)...
                    ', b = ' num2str(b) ', kappa = ' num2str(kappa) ...
                    ', j_nk = ' num2str(j_nk) ', U2 = ' num2str(U2) ...
                    ', x = ' num2str(x) ', h_a = ' num2str(h_a)...
                    ', h_f =' num2str(h_f) ', d_a = ' num2str(d_a)...
                    ', d_f = ' num2str(d_f) ', epsilon = ' num2str(epsilon)...
                    ', sig_H = ' num2str(Sig_H) ', sig_HP = ' ...
                    num2str(Sig_HP) ', sig_F = ' num2str(Sig_F) ...
                    ', sig_FP = ' num2str(Sig_FP)]);
                
                continueFlag = 1;
                continue;
            end
            
            %% VI.II Obliczenie sprzegla
            % I Przenoszony moment obrotowy - zginanie
            % 1. Wspolczynnik bezpieczenstwa - zaleznosc 81
            X_zj = 2.55;
            
            % 2. Naprezenie dopuszczalne - wzor 80
            k_gj = Stal(StalC45H,StalRe)/X_zj;
            
            % 3. Wspoczynnik koncentracji naprezen - PKM III str. 37
            K_u = 2.2;
            
            % 4. Wspolczynnik zarysu
            y = 0.272;
            
            % 4. Moment gnacy - wzor 79
            M_og = dp_m(i_dp)^2*dp_z(i_dp)*b*k_gj*(2+K1(i_dp))*y...
                /2/10^4/K_u*10^3;
            
            % 5. Sprawdzenie warunku 78
            if M_og < M_o
                disp(['Warunek (VI.I.I)5: dp = ' num2str(dp) ', m = ' ...
                    num2str(dp_m(i_dp)) ', z = ' num2str(dp_z(i_dp)) ...
                    ', z_g = ' num2str(z_g)...
                    ', b = ' num2str(b) ', kappa = ' num2str(kappa) ...
                    ', j_nk = ' num2str(j_nk) ', U2 = ' num2str(U2) ...
                    ', x = ' num2str(x) ', h_a = ' num2str(h_a)...
                    ', h_f =' num2str(h_f) ', d_a = ' num2str(d_a)...
                    ', d_f = ' num2str(d_f) ', epsilon = ' num2str(epsilon)...
                    ', sig_H = ' num2str(Sig_H) ', sig_HP = ' ...
                    num2str(Sig_HP) ', sig_F = ' num2str(Sig_F) ...
                    ', sig_FP = ' num2str(Sig_FP) ', M_og = ' num2str(M_og)]);
                
                continueFlag = 1;
                continue;
            end
            
            % II Przenoszony moment - warunek powierzchniowy
            % 1. Naprezenie dopuszczalne - wzor 83
            k_c = Stal(StalC45H,StalKc)/X_zj;
            
            % 2. Wspolczynnik obciazenia - wzor 84
            K2 = 54.05+(14.704*r0*sin(alpha)/b/sin(alpha))^2;
            
            % 3. Moment stykowy - wzor 82
            M_os = dp_m(i_dp)^2*dp_z(i_dp)*k_c*(2+K1(i_dp))*r0/...
                Stal(StalC45H,StalE)/K2^2/sin(alpha);
            
            
            disp(['[W] Wynik: dp = ' num2str(dp) ', m = ' ...
                num2str(dp_m(i_dp)) ', z = ' num2str(dp_z(i_dp)) ...
                ', z_g = ' num2str(z_g)...
                ', b = ' num2str(b) ', kappa = ' num2str(kappa) ...
                ', j_nk = ' num2str(j_nk) ', U2 = ' num2str(U2) ...
                ', x = ' num2str(x) ', h_a = ' num2str(h_a)...
                ', h_f =' num2str(h_f) ', d_a = ' num2str(d_a)...
                ', d_f = ' num2str(d_f) ', epsilon = ' num2str(epsilon)...
                ', sig_H = ' num2str(Sig_H) ', sig_HP = ' ...
                num2str(Sig_HP) ', sig_F = ' num2str(Sig_F) ...
                ', sig_FP = ' num2str(Sig_FP) ', M_o = ' num2str(M_o)...
                ', M_og = ' num2str(M_og)]);
            
            
            %% X Obliczenia gemetrii zeba
            % 1. Grubosc zeba - wzor 7
            s_kg = (pi/2+2*x*tan(alpha))*dp_m(i_dp);
            
            % 2. Luz wierzcholkowy - wzor 8
            c = c_wsp*dp_m(i_dp);
            
            % 3. Promien krzywizny wierzcholkowej - wzor 9
            rho_f_wsp = c_wsp/(1-sin(alpha));
            
            rho_f = rho_f_wsp*dp_m(i_dp);
            
            % 4. Promien beczulkowania zebow - wzor 15
            r01 = 0.2*dp;
            
            % 5. Dopuszczalny kat odchylenia - wzor 16
            a = 1*10^-3;
            alpha_dop = rad2deg(asin(2*r0*sin(alpha)/(b-2*a)));
            

            continueFlag = 0;
            break;
        end
        
        if continueFlag == 1
            continueFlag = 0;
            continue;
        end
        
        break;
    end
    
    if continueFlag == 1
        continueFlag = 0;
        continue;
    end
    break;
end

%% VII Obliczenie walu
% 1. Na skercanie - wzor 98
d_ws = (16*M_o/pi/Stal(StalC45H,StalKs))^(1/3);

% 2. Skrecanie
% 2.1 Modul kirchoffa - wzor 100
G = Stal(StalC45H,StalE)/2/Ni;

% 2.2 Srednica walu - wzor 103
d_wf = (32*M_o*L/G/pi/Fi_dop)^(1/4);

% 3. Wybranie srednicy walu
d_w_max = max(d_ws,d_wf);

% 4. Standaryzacja srednicy walu
for i = 1:d_standard_len
    if d_standard(i) > d_w_max
        d_w = d_standard(i);
        break;
    end
end

if dp < d_w
    disp('Warunek VII');
end

%% VIII Obliczenie wpustu
% 1. Wysokosc wpustu
h = d_w/2;

% 2. Dlugosc wpustu
l = b*0.9;

% 3. Liczba wpustow
ilosc_wpustow = 0;
for j = 1:3
    % 2. Sila jednostkowa - wzor 107
    N_i = 4*M_o/h/l/j/d_w;
    
    % 3. Warunek 108
    if N_i < Stal(StalC45H,StalKd)
        ilosc_wpustow = j;
        break;
    end 
end

disp(['[W] Wynik: h = ' num2str(h) ', l = ' num2str(l) ', N_i = '...
    num2str(N_i) ', j = ' num2str(ilosc_wpustow)]);

%% IX Obliczenie srub
% 1. Srednica kolnierza
D_min = dp*1.5;

for i = 1:d_standard_len
    if d_standard(i) > D_min
        D_k = d_standard(i);
        break;
    end
end

% 2. Sila obwodowa na kolnierzu - wzor 72
P_wk = 19100*N*10^-3/n/D_k;

% 3. Wspolczynnik tarcia stal-stal
mi_k = 0.7;

% 4. Wstepne napiecie sruby - wzor 112
N_k = P_wk/mi_k*1.25;

% 5. Szerokosc kolnierza
s_k = (D_k - d_a)/2;

% 6. Maksymalna srednica sruby
d_3max = s_k/5;

% 7. Liczba srub
for l_s = 4:4:32

    % 8. Srednca z analizy naprezeniowej - wzor 116
    d_3n = (5*P_wk/pi/Stal(StalC45,StalKr)/l_s/mi_k)^(1/2);
    
    % 9. Srednica z analizy zredukowanej - wzor 120
    kappa_H = 3;
    d_3r = (4*P_wk/pi/Stal(StalC45,StalKs)/l_s*((1.25/mi_k)^2+kappa_H)^(1/2))^(1/2);
   
    % 10. Warunek srednicowy
    if (d_3max > d_3n) && (d_3max > d_3r)
        d_3_min = max(d_3n,d_3r);
        break;
    end
end

% 11. Powierzchnia uszczelki
A_r = pi*(D_k-dp)^2/4;

% 12. Sila docisku uszczelki
q_u = 7*10^5;

% 13. Napiecie poczatkowe - wzor 121
N_0 = q_u*A_r;

% 14. Nacisk maksymalny - wzor 122
Q = max(N_k,N_0);

% 15. Kat rozwarcia gwintu
alpha_g = deg2rad(60);

% 16. Pozorny kat tarcia - wzor 124
rho_p = atan(mi_k/cos(alpha_g));

% 17. Podzialka M18
P_g = 1*10^-3;
d_2k = 9.428*10^-3;
d_3 = 10*10^-3; 
r_sr = (d_3+d_2k)/2;

% 18. Kat wzniosu gwintu - wzor 125
gamma = atan(P_g/pi/d_2k);

% 19. Moment dokrecenia sruby - wzor 123
M_dok = Q/l_s*(d_3/2*tan(gamma+rho_p)+mi_k*r_sr);