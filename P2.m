clear all


%% I Zalozenia
Q = 18000;  % [N] obciazenie sruby
H = 0.3;    % [m] wysokosc podnoszenia 
F_r = 200;  % [N] sila czlowieka

%% II Rozpatrywane stale 
%       Re - Rm - E - Kr - Kc - Ks - Kgj - Kh
%   M1
%   M2
%   ...
%
% Re - granica plastycznosci [Pa]
% Rm - wytrzymalosc na rozciaganie [Pa]
% Kr - naprezenie dopuszczalne na stale rozciaganie [Pa]
% Kc - naprezenie dopuszczalne na stale sciskanie [Pa]
% Ks - naprezenie dopuszczalne na stale skrecanie i sciskanie [Pa]
% Kgj - naprezenie dopuszczalne na jednostronne zginanie tnace [Pa]
% Kh - naprezenia dopuszczalne [Pa]

% 1. Definicja podstawowych wlasnosci stali [Re - Rm - E]
StalReRmE = [320*10^6, 550*10^6, 210*10^9;      % C15
    490*10^6, 700*10^6, 210*10^9;               % C45
    230*10^6, 750*10^6, 209*10^9;               % 1.4301          
    240*10^6, 680*10^6, 209*10^9];              % 1.4401

% 2. Wyznaczenie naprezen krytycznych
LiczbaStali = length(StalReRmE(:,1));

Kr = 0.55;
Kc = 0.55;
Ks = 0.33;
Kgj = 0.35;
Kh = 950*10^6;

% 3. Macierz pelnych wlasnosci stali
Stal=[];
for i = 1:LiczbaStali
    Stal(i,:) = [StalReRmE(i,1), StalReRmE(i,2), StalReRmE(i,3), ...
        StalReRmE(i,1)*Kr, StalReRmE(i,1)*Kc, StalReRmE(i,1)*Ks, ...
        StalReRmE(i,1)*Kgj, Kh];
end

%% III Obliczenie minimalnej srednicy rdzenia d_3 ze wzoru 6
d_3_min_6=[];

for i = 1:LiczbaStali
    d_3_min_6(i) = (4*Q/pi/Stal(i,5))^(1/2);
end

%% IV Obliczenie minimalnej srednicy rdzenia d_3 ze wzoru 13
% 1. Obliczenie dlugosci wyboczeniowej sruby - wzor 10 i 11
H_n = 0.03; % [m] wysokosc nakretki
H_p = 0.03; % [m] wysokosc osadzenia

l_s = H+H_n/2+H_p;
l_w = l_s*0.8;

d_3_min_13=[];
for i = 1:LiczbaStali
    d_3_min_13(i) = (64*Q*l_w^2/pi^3/Stal(i,3))^(1/4);
end

%%. V Obliczenie minimalnej srednicy rdzenia d_3 ze wzgledu na smuklosc
% 1. Eulerowska smuklosc graniczna R_H = R_e - wzor 22
lambda_H=[];
for i = 1:LiczbaStali
    lambda_H(i) = pi*(Stal(i,3)/Stal(i,1))^(1/2);
end

% 2. Smuklosc preta dla wyboczeniowej srednicy rdzenia d_3 
lambda_w=[];
for i = 1:LiczbaStali
    lambda_w(i) = 4*l_w/d_3_min_13(i);
end

% 3. Sprawdzenie warunku wyboczenia sprezystego - wzor 23
n_zal = 8;

wyb_spr=[];
for i = 1:LiczbaStali
    wyb_spr(i) = (lambda_w(i) >= lambda_H(i)*n_zal^(1/2));
end

% 4. Obliczenie srednicy rdzenia sruby d_3 w zaleznosci od warunku 23 -
% wzor 29
d_3_min_29=[];
for i = 1:LiczbaStali
    if wyb_spr(i)
        d_3_min_29(i) = d_3_min_13(i);
    else
        d_3_min_29(i) = (n_zal/Stal(i,1))^(1/2)*...
            (4*Q/pi + Stal(i,1)^2*4*l_w^2/Stal(i,3)/pi^2/n_zal)^(1/2);
    end
end

% 5. Przyporzadkowanie gwintow do znormalizowanych wartosci
d_nom = [[10:2:32], [36:4:44], 48, 50, 60, 70]*10^-3;
d_2_nom = [8.739, 10.191, 12.191, 13.64, 15.64, 17.64, 19.114, 21.094,...
    23.094 25.094, 26.547, 28.547, 32.547, 36.02, 40.02, 43.468,...
    45.468, 54.935, 64.425]*10^-3;
d_3_nom = [6.89, 7.84, [8.8:2:14.8], [15.5:2:21.5], 21.9, 23.9, 27.9,...
    30.5, 34.5, 37.8, 39.3, 48.15, 57]*10^-3;
P_gwintu = [2, 3, 3, 4,4,4,5,5,5,5,6,6,6,7,7,8,8,9,10]*10^-3;
H_nkr = [25,35,35,35,35,44,44,44,46,46,46,54,54,66,66,75,75,90]*10^-3; 
% 6. Sprawdzenie poprawnosci wprowadzenia
iloscSrednic = length(d_nom);

if (iloscSrednic ~=  length(d_3_nom)) && ...
        (iloscSrednic ~=  length(d_2_nom)) && ...
        (iloscSrednic ~=  length(P_gwintu)) && ...
        (iloscSrednic ~=  length(H_nkr))
    pause;
end

% 7. Dopasowanie srednic rdzenia d_3 do nominalnych
d_3_dopasowane=[];
d_2_dopasowane=[];
d_dopasowane=[];
P_dopasowane=[];
H_dopasowane=[];
for i = 1:LiczbaStali
    for j = 1:iloscSrednic
        if d_3_nom(j) > d_3_min_29(i)
            d_3_dopasowane(i) = d_3_nom(j);
            d_2_dopasowane(i) = d_2_nom(j);
            d_dopasowane(i) = d_nom(j);
            P_dopasowane(i) = P_gwintu(j);
            H_dopasowane(i) = H_nkr(j);
            break;
        end
    end
end

% 8. Sprawdzenie wsp. bezpieczenstwa - wzor 25
sigma_n=[];
sigma_H=[];
n=[];

for i = 1:LiczbaStali
    sigma_n(i) = 4*Q/pi/d_3_min_29(i)^2;
    sigma_H(i) = pi^2*Stal(i,3)/lambda_H(i)^2;
    n(i)=sigma_H(i)/sigma_n(i);
end


%% VI Okreslenie badanych gwintow
%  kat:     gora - dol
%   G1
%   G2
%   ...
%
% kat gora - kat rozwarcia gwintu w gornej czesci zeba
% kat dol - kat rozwarcia gwintu w dolnej czesci zeba

% 1. Definicja katow rozwarcia gwintow
GwintSt = [15, 15;  % Tr
    30, 3;          % S
    3, 30;];        % S

% 2. Okreslenie liczby zdefiniowanych gwintow
LiczbaGwintow = length(GwintSt(:,1));

% 3. Opisanie katow w radianach
GwintRad=[];
for i = 1:LiczbaGwintow
    for j = 1:2
        GwintRad(i,j) = deg2rad(GwintSt(i,j));
    end
end

%% VII Wyznaczenie momentow tarcia
% 1. Sila tarcia - wzor 32
%   kier. gora - dol
%   mi_1'
%   mi_2'
%   ...

mi_prim=[];
T=[];

mi = 0.1;   % wsp. tarcia Stal-Cu
for i = 1:LiczbaGwintow
    for j = 1:2
        mi_prim(i,j) = mi/cos(GwintRad(i,j));
    end
end

for i = 1:LiczbaGwintow
    for j = 1:2
        T(i,j) = Q* mi_prim(i,j);
    end
end

% 2. Obliczenie pozornego kata tarcia - wzor 33
rho_prim=[];
for i = 1:LiczbaGwintow
    for j = 1:2
        rho_prim(i,j) = atan(mi_prim(i,j));
    end
end

% 3. Obliczenie kata wzniosu gwintu - wzor 34
gamma=[];
for i = 1:LiczbaStali
    gamma(i) = atan(P_dopasowane(i)/pi/d_2_dopasowane(i));
end

% 4. Dopasowanie srednicy gwintu d_2 do warunku 36
war_d_2=[];
for i = 1:LiczbaGwintow
    war_d_2(i) = atan(mi/cos(GwintRad(i,2)));
end

for i = i:LiczbaStali
    for j = 1:LiczbaGwintow
        if gamma(i) >= war_d_2(j)
            for k = 1:iloscSrednic
                if d_2_nom(k) > d_2_dopasowane(i)
                    d_3_dopasowane(i) = d_3_nom(k);
                    d_2_dopasowane(i) = d_2_nom(k);
                    d_dopasowane(i) = d_nom(k);
                    P_dopasowane(i) = P_gwintu(k);
                end
            end
        end
    end
end

% 5. Wyznaczenie momentu tarcia sruba-nakretka - wzor 37
M_T_1=[];
kat_t=[];
for i = 1:LiczbaStali
    for j = 1:LiczbaGwintow
        for k = 1:2
            kat_t(i,j,k) = rad2deg(gamma(i)+(-1)^k*rho_prim(j,k));
            M_T_1(i,j,k) = d_dopasowane(i)/2*Q*tan(gamma(i)+...
                (-1)^k*rho_prim(j,k));
        end
    end
end

% 6. Obliczenie tarcia sruba-support - wzor 38
% r_sr = (d-d_2)/2
r_sr = (d_dopasowane + d_2_dopasowane)./2;

M_T_2=[];
for i = 1:LiczbaStali
    M_T_2(i) = Q*mi*r_sr(i);
end

% 7. Obliczenie tarcia calkowitego - wzor 39
M_T=[];
for i = 1:LiczbaStali
    for j = 1:LiczbaGwintow
        for k = 1:2
            M_T(i,j,k) = M_T_1(i,j,k)+M_T_2(i);
        end
    end
end

%% VIII Obliczenie sprawnosci gwintu
eta=[];
for i = 1:LiczbaStali
    for j = 1:LiczbaGwintow
        for k = 1:2
            eta(i,j,k) = tan(gamma(i))/tan(gamma(i)+rho_prim(j,k));
        end
    end
end

%% IX Hipoteza Hubera-Misesa
% 1. Naprezenia maksymalne - wzor 44
sigma_max=[];
for i = 1:LiczbaStali
    sigma_max(i) = 4*Q/pi/d_3_dopasowane(i)^2;
end

% 2. Moment skrecajacy - wzor 43
tau_s=[];
for i = 1:LiczbaStali
    for j = 1:LiczbaGwintow
        for k = 1:2
            tau_s(i,j,k) = 32*M_T(i,j,k)/pi/d_2_dopasowane(i)^3;
        end
    end
end

% 3. Naprezenie zredukowane - wzor 47
sigma_red=[];
for i = 1:LiczbaStali
    for j = 1:LiczbaGwintow
        for k = 1:2
            sigma_red(i,j,k) = (sigma_max(i)^2 + 3*tau_s(i,j,k)^2)^(1/2);
        end
    end
end

% 4. Sprawdzenie warunku 48
war_48=[];
war_48_sig=[];
for i = 1:LiczbaStali
    for j = 1:LiczbaGwintow
        for k = 1:2
            war_48_sig(i,j,k) = min(Stal(i,4),Stal(i,5));
            war_48(i,j,k) = sigma_red(i,j,k) >= min(Stal(i,4),Stal(i,5));
        end
    end
end

%% X Promien krzywizny
% 1. Promien - wzor 49
R=[];
for i = 1:LiczbaStali
    R(i) = (0.388^3*Q*Stal(i,3)^2/Stal(i,8)^3)^(1/2);
end

% 2. Srednica kontaktu - wzor 50
d_0=[];
for i = 1:LiczbaStali
    d_0(i) = 2.2*(Q*R(i)/Stal(i,3))^(1/2);
end

% 3. Moment tarcia - wzor 51
M_t=[];
for i = 1:LiczbaStali
    M_t(i) = 1/3*Q*d_0(i)*mi;
end

%% XI Nakretka
% 1. Krotnosc gwintu
n_gwintu = [1,1,1,1];

if length(n_gwintu) ~= LiczbaStali
    pause;
end

% 2. Skok gwintu - wzor 53
P_n=[];
for i = 1:LiczbaStali
    P_n(i) = P_dopasowane(i)*n(i);
end

% 3. Wysokosc nakretki - wzor 54
P_dop = 120*10^6;    % [Pa] Naprezenie dopuszczalne 

H_N=[];
for i = 1:LiczbaStali
    H_N(i) = 4*Q*P_n(i)/pi/(d_dopasowane(i)^2-d_3_dopasowane(i)^2)/P_dop;
end

for i = 1:LiczbaStali
    if H_N(i) > H_dopasowane(i)
        pause;
    end
end

% 4. Dobre prowadzenie - wzor 55
z=[];
for i = 1:LiczbaStali
    z(i) = H_N(i)/P_n(i);
end

% 4. Dobre prowadzenie calkowite
z_c=[];
for i = 1:LiczbaStali
    z_c(i) = ceil(z(i));
end

%% XII Drazek
% 1. Dlugosc - wzor 57
l_d=[];
for i = 1:LiczbaStali
    for j = 1:LiczbaGwintow
        for k = 1:2
            l_d(i,j,k) = M_T(i,j,k)/F_r;
        end
    end
end

% 2. Srednica obsady - wzor 58
d_z=[];
for i = 1:LiczbaStali
    d_z(i) = 1.2*d_dopasowane(i);
end

% 3. Moment gnacy - wzor 60
M_g=[];
for i = 1:LiczbaStali
    for j = 1:LiczbaGwintow
        for k = 1:2
            M_g(i,j,k) = F_r*(l_d(i,j,k)-0.5*d_z(i));
        end
    end
end

% 4. Srednica minimalna - wzor 59
d_n=[];
for i = 1:LiczbaStali
    for j = 1:LiczbaGwintow
        for k = 1:2
            d_n(i,j,k) = (32*M_g(i,j,k)/pi/Stal(i,7))^(1/3);
        end
    end
end

% 5. Srednica z nacisku - wzor 62
d_N=[];
for i = 1:LiczbaStali
    for j = 1:LiczbaGwintow
        for k = 1:2
            d_N(i,j,k) = 6*M_T(i,j,k)/d_z(i)^2/P_dop;
        end
    end
end

% 6. Dopasowanie srednicy drazka
d_N_bezp = 5*d_N;
d_N_dopasowane=[];
for i = 1:LiczbaStali
    for l = 1:2
        for j = 1:LiczbaGwintow
            for k = 1:iloscSrednic
                if d_nom(k) > d_N_bezp(i,j,l)
                    d_N_dopasowane(i,j,l) = d_nom(k);
                    break;
                end
            end
        end
    end
end  
